import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class DoubleStack {

   public List<Double> getList() {
      return list;
   }

   private List<Double> list = new LinkedList<Double>();

   public static void main (String[] argum) {
      System.out.println(interpret("         "));
   }

   /**
    * Constructor for a new stack
    */
   DoubleStack() { }

   DoubleStack(List<Double> list) {
      this.list = list;
   }

   /**
    * Copy of the stack
    * @return clone of the stack
    * @throws CloneNotSupportedException
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new DoubleStack(new LinkedList<Double>(getList()));
   }

   /**
    * Check whether the stack is empty
    * @return true if stack is empty, false otherwise
    */
   public boolean stEmpty() {
      return list.isEmpty();
   }

   /**
    * Adds an element to the stack
    * @param a as double
    */
   public void push (double a) {
      list.add(a);
   }

   /**
    * Removes the most recently added element that was not yet removed
    * @return last position in the stack as double
    */
   public double pop() {
      if (this.stEmpty()) throw new IndexOutOfBoundsException("Cannot pop - List is empty");
      return list.remove(list.size() - 1);
   }

   /**
    * Arithmetic operation between two topmost elements of the stack (result is left on top)
    * @param s as operation +, -, * or /
    */
   public void op (String s) {
      if (!isOperand(s)){
         throw new IllegalArgumentException(s + " on keelatud tehtemärk.");
      } else {
         if (list.size() >= 1) {
            Double num1 = pop();
            Double num2 = pop();

            switch (s.charAt(0)) {
               case '+':
                  push(num2 + num1);
                  break;
               case '-':
                  push(num2 - num1);
                  break;
               case '*':
                  push(num2 * num1);
                  break;
               case '/':
                  push(num2 / num1);
                  break;
            }
         } else {
            throw new IndexOutOfBoundsException("Pole piisavalt arve stackis");
         }
      }
   }

   /**
    * Read the top without removing it
    * @return value of top element as double
    */
   public double tos() {
      if (this.stEmpty()) throw new IndexOutOfBoundsException("Cannot read the top - List is empty");
      return list.get(list.size() - 1);
   }

   /**
    * Check whether two stacks are equal
    * @param o as DoubleStack object
    * @return boolean returns true if objects are equal
    */
   @Override
   public boolean equals (Object o) {
      return getList().equals(((DoubleStack)o).getList());

      //return o.equals(this); // Works also, but do not understand why (different objects are compared, List vs DoubleStack?) (Only in this direction)
      //return this.equals(o);  // This direction does not work
   }

   /**
    * Conversion of the stack to string
    * @return list as string
    */
   @Override
   public String toString() {
      return list.toString();
   }

   /**
    * Calculate the value of an arithmetic expression in RPN (Reverse Polish Notation)
    * @param pol as  string which contains double numbers (including negative and multi-digit numbers) and arithmetic operations + - * / separated by whitespace symbols
    * @return double value of the expression
    * @throws RuntimeException in case the expression is not correct
    */
   public static double interpret (String pol) {
      DoubleStack ds = new DoubleStack();
      String[] split = pol.replaceAll("\t", " ").replaceAll(" +", " ").trim().split(" ");

      if(pol.trim().length() == 0) throw new RuntimeException("Tühi sisend");

      if((isDouble(split[split.length - 1]) && split.length > 1)) throw new RuntimeException("Sisendi \"" + pol + "\" viimane element ei või olla number");

         for (String m : split) {
            if (isDouble(m)) {
               ds.push(Double.parseDouble(m));
            } else if (isOperand(m)) {
               ds.op(m);
            } else throw new RuntimeException("Sisend \"" + pol + "\" sisaldab keelatud sümboleid.");
         }
      
      return ds.tos();
   }

   /**
    * Check if a String is a Double
    * @param s as String that is checked if it is a Double value
    * @return boolean, true if String is a Double
    */

   public static boolean isDouble(String s) {
      try {
         Double.parseDouble(s);
         return true;
      } catch (NumberFormatException e) {
         return false;
      }
   }

   /**
    * Check if a String is an operand
    * @param s as String that is checked
    * @return boolean, true if String is a predefined operand
    */
   public static boolean isOperand(String s) {
      return Arrays.asList("+", "-", "*", "/").contains(s);
   }
}